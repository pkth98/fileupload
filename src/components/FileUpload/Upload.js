import React, { Component } from "react";
import Dropzone from "../dropzone/Dropzone";
//import Dropzone from 'react-dropzone';
import "./Upload.css";


class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      files: [],
    };

    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.renderActions = this.renderActions.bind(this);
  }

  onFilesAdded(files) {
    this.setState(prevState => ({
      files: prevState.files.concat(files)
    }));
  }


  

  renderActions() {
    
      return (
        <div>
        
        <div className="btn-group">
        <button
          
          
        ><span class="icon1"></span>
           Import from Google Drive
        </button>
        <button
          
         
        >
          <span class="icon2"></span>
          Import from Dropbox
        </button>
        
        </div>
        </div>
      );
    //}
  }
  onDrop = (acceptedFiles) => {
    console.log(acceptedFiles);
  }
  

  render() {
    return (
      <div className="App">
        <div className="Card">
      <div className="Upload">
         
        <div className="Content">
          <div>
          <Dropzone
      onFilesAdded={this.onFilesAdded}
      disabled={this.state.uploading || this.state.successfullUploaded}/>
          </div>
          <div className="Files">
            {this.state.files.map(file => {
              return (
                <div key={file.name} className="Row">
                  <span className="Filename">{file.name}</span>
                 
                </div>
              );
            })}
          </div>
        </div>
        <div className="Actions">{this.renderActions()}</div>
        <div className="vl1"><h6 className="or">OR</h6></div>
        
        <div className="vl2"></div>
       
      </div>
      </div>
      </div>
      
    );
  }
}

export default Upload;

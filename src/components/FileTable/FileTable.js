import React from 'react';


import { Container, Row, Col,Table} from "reactstrap";
//import "bootstrap/dist/css/bootstrap.css";
import TagsInput from "../TagsInput/TagsInput";


import Select, { createFilter } from "react-select";
import './FileTable.css';
const FileTable=()=> {
    

  const data=[{Preview:"Image",SKU:"heb",AssetName:"fwefw",CountryAccess:"fhef",Metadata:"vkgfw",Tags:"dkwf",Categories:"dnk"},
  {Preview:"Image",SKU:"heb",AssetName:"fwnefwjfjnvjefw",CountryAccess:"fhef",Metadata:"vkgffwvabsvj sh, vhjbvs fbvgerkfvbhbvesbv bwuirhh vuerhgfabshvx vsdbfuwhbgjhrfwhbf2iu3ry2bja bn dvhsrbgfuana vshbvsrgfa sfbaefnghuiafbcsfvamnf vjehfblafv hrbfguliqfbahkgfbasv skegfafbweugfq3yhfa vuerhgvywhfanhjvbsenrgjfeuigfbbvsvnifh wiughayfvbs ughqgba fsgnw3bfiy hfwbgfu iw hfgugshbvss",Tags:"dkwf",Categories:"dnk"},
  {Preview:"Image",SKU:"heb",AssetName:"fwnefwjfjnvjefw",CountryAccess:"fhef",Metadata:"vkgffwvabsvj sh, vhjbvs fbvgerkfvbhbvesbv bwuirhh vuerhgfabshvx vsdbfuwhbgjhrfwhbf2iu3ry2bja bn dvhsrbgfuana vshbvsrgfa sfbaefnghuiafbcsfvamnf vjehfblafv hrbfguliqfbahkgfbasv skegfafbweugfq3yhfa vuerhgvywhfanhjvbsenrgjfeuigfbbvsvnifh wiughayfvbs ughqgba fsgnw3bfiy hfwbgfu iw hfgugshbvss",Tags:"dkwf",Categories:"dnk"},
  {Preview:"Image",SKU:"heb",AssetName:"fwnefwjfjnvjefw",CountryAccess:"fhef",Metadata:"vkgffwvabsvj sh, vhjbvs fbvgerkfvbhbvesbv bwuirhh vuerhgfabshvx vsdbfuwhbgjhrfwhbf2iu3ry2bja bn dvhsrbgfuana vshbvsrgfa sfbaefnghuiafbcsfvamnf vjehfblafv hrbfguliqfbahkgfbasv skegfafbweugfq3yhfa vuerhgvywhfanhjvbsenrgjfeuigfbbvsvnifh wiughayfvbs ughqgba fsgnw3bfiy hfwbgfu iw hfgugshbvss",Tags:"dkwf",Categories:"dnk"},
  
  {Preview:"Image",SKU:"heb",AssetName:"fwnefwjfjnvjefw",CountryAccess:"fhef",Metadata:"vkgffwvabsvj sh, vhjbvs fbvgerkfvbhbvesbv bwuirhh vuerhgfabshvx vsdbfuwhbgjhrfwhbf2iu3ry2bja bn dvhsrbgfuana vshbvsrgfa sfbaefnghuiafbcsfvamnf vjehfblafv hrbfguliqfbahkgfbasv skegfafbweugfq3yhfa vuerhgvywhfanhjvbsenrgjfeuigfbbvsvnifh wiughayfvbs ughqgba fsgnw3bfiy hfwbgfu iw hfgugshbvss",Tags:"dkwfwjuihfrfasjfabfkufvjcbjhbfkjbrjskbfywbrfhsbvhjcbhdbashjbflqubeyfjhbsfbwbfswryjbfjwbvfwfbkuefbkuabfu",Categories:"dnk"}];
  
  const filterConfig = {
    ignoreCase: true,
    ignoreAccents: true,
    trim: true,
    matchFrom: "start"
  };
  const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'vuna', label: 'Vuna' },
    { value: 'sour', label: 'Sour' },
    
  ]
  const countries = [
    { value: 'india', label: 'India' },
    { value: 'usa', label: 'USA' },
    { value: 'brazil', label: 'Brazil' },
    { value: 'peru', label: 'Peru' },
    { value: 'france', label: 'France' },
    
  ]
  

  
  const render=(data,index)=>{
   
   
    return(
      <tr key={index}  >
        <td><input type="checkbox"></input></td>
        <td>{data.Preview}</td>
        <td width="200px">  <Select closeMenuOnSelect={false} isMulti name="colors" options={options}  filterOption={createFilter(filterConfig)}/></td>
        
        <td>{data.AssetName}</td>
        <td width="200px"> <Select closeMenuOnSelect={false} isMulti name="colors" options={countries}  filterOption={createFilter(filterConfig)}/></td>
        <td> <textarea placeholder="Write metadata here..." rows="3" cols="20"></textarea></td>
        <td width="200px"><TagsInput selectedTags={selectedTags}  tags={[]}/></td>
        <td>{data.Categories}</td>
        <td><div><button ><span class="attachicon"></span>Attach License File</button></div><div><textarea className="input" placeholder="Write description here..." rows="3" cols="20"></textarea></div></td>



      </tr>
    )

  }
  const selectedTags = tags => {
    console.log(tags);
};
  
  return (
    
   <div>
     
     <div className="Background">
      <Table striped bordered hover className="table">
  <thead>
    <tr>
      
      <th><input type="checkbox"></input></th>
      <th>Preview</th>
      <th >SKU</th>
      <th>Asset Name</th>
      <th>Country Access</th>
      <th>Metadata</th>
      <th>Tags</th>
      <th>Categories</th>
      <th>Other Details</th>
    </tr>
  </thead>
  <tbody>
   {data.map(render)}
  </tbody>
  
</Table>
  <button className="cancel">Cancel</button>
  <button className="uploadButton">Upload</button>
    
    </div>
    </div>
   
    
);
  

  }

export default FileTable;
